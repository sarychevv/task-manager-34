package ru.t1.sarychevv.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.repository.ISessionRepository;
import ru.t1.sarychevv.tm.api.service.ISessionService;
import ru.t1.sarychevv.tm.model.Session;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final ISessionRepository repository) {
        super(repository);
    }

    @Override
    public @Nullable Session create(@Nullable String userId, @Nullable String name, @Nullable String description) {
        return null;
    }

}


