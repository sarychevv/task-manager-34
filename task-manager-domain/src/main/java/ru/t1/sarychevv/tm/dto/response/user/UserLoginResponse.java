package ru.t1.sarychevv.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class UserLoginResponse extends AbstractUserResponse {

    public UserLoginResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

    public UserLoginResponse(@Nullable final String token) {
        super(token);
    }

}
