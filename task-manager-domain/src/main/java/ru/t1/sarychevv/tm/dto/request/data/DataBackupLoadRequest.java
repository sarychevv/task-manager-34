package ru.t1.sarychevv.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.request.AbstractUserRequest;

public class DataBackupLoadRequest extends AbstractUserRequest {

    public DataBackupLoadRequest(@Nullable final String token) {
        super(token);
    }

}
