package ru.t1.sarychevv.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.endpoint.IAuthEndpoint;
import ru.t1.sarychevv.tm.api.endpoint.ITaskEndpoint;
import ru.t1.sarychevv.tm.command.AbstractCommand;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.exception.field.ProjectEmptyException;
import ru.t1.sarychevv.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    public ITaskEndpoint getTaskEndpoint() {
        return serviceLocator.getTaskEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    public IAuthEndpoint getAuthEndpoint() {
        return serviceLocator.getAuthEndpoint();
    }

    protected void renderTasks(@NotNull final List<Task> tasks) {
        int index = 0;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getId() + ": " + task.getName() +
                    " " + task.getDescription() + " " + Status.toName(task.getStatus()));
            index++;
        }
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) throw new ProjectEmptyException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
